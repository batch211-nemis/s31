// 1. What directive is used by Node.js in loading the modules it needs?

		//Answer: REQUIRE DIRECTIVE

// 2. What Node.js module contains a method for server creation?

		//Answer: HTTP MODULE

// 3. What is the method of the http object responsible for creating a server using Node.js?

		//Answer: createServer() Method

// 4. What method of the response object allows us to set status codes and content types?

		//Answer: writeHead() Method

// 5. Where will console.log() output its contents when run in Node.js?

		//Answer: TERMINAL (Git Bash)

// 6. What property of the request object contains the address's endpoint?

		//Answer: URL